```
├── README.md
├── models
│   ├── addition-task.js
│   ├── comparison-task.js
│   ├── comparisonnumber-task.js
│   ├── correspondence-task.js
│   ├── counting-task.js
│   ├── level-game.js
│   ├── number-identification.js
│   ├── numberline-task.js
│   ├── ordinal-task.js
│   ├── test-task.js
│   ├── time-limit.js
│   └── user.js
├── package-lock.json
├── package.json
├── routes
│   ├── addition-task.js
│   ├── comparison-task.js
│   ├── comparsionnumber-task.js
│   ├── correspondence-task.js
│   ├── couting-task.js
│   ├── level-games.js
│   ├── number-iden.js
│   ├── numberline-task.js
│   ├── ordinal-task.js
│   ├── test-task.js
│   ├── time-limits.js
│   └── users.js
└── server.js
```
วิธีการติดตั้ง
1. ติดตั้ง nodeJS โดยดาวโหลดได้ที่ https://nodejs.org/en/download/
2. ติดตั้ง mongoDB โดยดาวโหลดได้ที่ https://www.mongodb.org/downloads และวิธีติดตั้งตามระบบปฏิบัติการ https://docs.mongodb.com/manual/administration/install-community/
3. ทำการเพิ่มฐานข้อมูลโดยเข้าไปที่ cmd หรือ terminal พิมพ์ mongo เพื่อเข้าสู่ mongo shell
4. สร้างฐานข้อมูลพิมพ์คำสั่ง use <ชื่อฐานข้อมูล>
5. ทำการสร้าง User admin สำหรับฐ้านข้อมูลที่พึ่งสร้างขึ้นโดยคำสั่ง db.createUser(
  {
    user: "<ชื่อ user>",
    pwd: "<รหัสผ่าน>",
    roles: [ { role: "readWrite", db: "<ชื่อฐานข้อมูล>" } ]
  }
) จากนั้นพิม exit เพื่อออกจาก mongo shell
6. ดาวโหลดโฟเดอร์นี้โดย git clone https://gitlab.com/chaiyootcass/smart_math_nodejs_mongodb.git หรือดาวโหลดที่ https://gitlab.com/chaiyootcass/smart_math_nodejs_mongodb
7. เปิด cmd หรือ terminal เข้าไปที่โฟเดอร์พิมพ์คำสั่ง npm install
8. แก้ไขไฟล์ server.js ที่ตัวแปร URL เปลี่ยนเป็น 'mongodb://127.0.0.1:27017/<ชื่อฐานข้อมูล>';
9. แก้ไข User ที่ตัวแปร options ตรง user และ pass ให้ตรงกับที่สร้างไว้ในข้อที่ 5
10. พิมพ์คำสั่ง npm run start เพื่อเริ่มเปิดเซิพเวอร์