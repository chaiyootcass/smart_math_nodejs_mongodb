const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const URL = 'mongodb://127.0.0.1:27017/smart_math';
const options = {
  user: 'smartMath',
  pass: '402',
  useNewUrlParser: true,
  useUnifiedTopology: true
};

const userRoutes = require('./routes/users');
const levelGame = require('./routes/level-games');
const numberIden = require('./routes/number-iden');
const comparisonTask = require('./routes/comparison-task');
const timelimit = require('./routes/time-limits');
const ordinalTask = require('./routes/ordinal-task');
const additionTask = require('./routes/addition-task');
const correspondenceTask = require('./routes/correspondence-task');
const numberlineTask = require('./routes/numberline-task');
const comparisonNumberTask = require('./routes/comparsionnumber-task');
const countingTask = require('./routes/couting-task');
const testTask = require('./routes/test-task');

mongoose.set('useFindAndModify', false);
mongoose.connect(URL, options, function(err, result) {
  if (err) {
    throw err;
  }else {
    console.log('MongoDB Connected');
  }
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use('/user',userRoutes);
app.use('/numberident_task',numberIden);
app.use('/addition_task',additionTask);
app.use('/ordinal_task',ordinalTask);
app.use('/comparison_task',comparisonTask);
app.use('/correspondence_task',correspondenceTask);
app.use('/numberline_task',numberlineTask);
app.use('/comparisonnumber_task',comparisonNumberTask);
app.use('/counting_task',countingTask);
app.use('/test_task',testTask);
app.use('/levelgame',levelGame);
app.use('/timelimit',timelimit);
app.listen(8080, () => {
    console.log("Server Started");
});
