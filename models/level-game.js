const mongoose = require('mongoose');

const levelGame = mongoose.Schema({
    game: { type: String, require: true, enum: ['numberident_task', 'counting_task','counting_task'
    ,'comparison_task','addition_task',
    'ordinal_task','correspondence_task','numberline_task'] },  //ชื่อเกมส์
    numLevel: { type: Number }, //จำนวนเวเวลทั้งหมด
    level: { type: [Object], require: true },  //ข้อมูลรายระเอียดของแต่ละเลเวล เช่น จำนวนข้อ เลขน้อยสุด มากสุด
}, {
    timestamps: true,
})

module.exports = mongoose.model('levelGame', levelGame);