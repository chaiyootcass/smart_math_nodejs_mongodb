const mongoose = require('mongoose');

const AdditionTask = mongoose.Schema({
    userid: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },// ไอดีของผู้เล่น *String นะ
    level: { type: Number }, //ระดับเลเวล
    time: { type: Number }, //เวลาทั้งหมดที่ทำต่อ 1 ชุด
    correct: { type: Number }, // จำนวนข้อที่ทำถูก
    fail: { type: Number }, // จำนวนข้อที่ทำผิด
    solve: { type: [Number] }, // Array ของ โจทย์
    answer: { type: [Number] }, // Array ของคำตอบ ที่ผู้เล่นตอบ
    note:{ type:String, default: 'Level up' }, //หมายเหตุ ว่ามีการเพิ่มเลเวลหรือลดลง
    createdAt: {
        type: Date,
        default: Date.now
    },                                     //ข้อมูลที่เพิ่มขึ้นเมื่อไร
})

module.exports = mongoose.model('addition_log', AdditionTask);