const mongoose = require('mongoose');

const TestTask = mongoose.Schema({
    userid: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },// ไอดีของผู้เล่น *String นะ
    log: { type:[Object] }, //เป็น type Opject เก็บข้อมูลสถิติเกมส์นั้นๆ จะเก็บเช่น ชื่อเกมส์ คะแนน เวลา
    time: { type: Number }, //เวลาทั้งหมดที่ใช้ในการทำแบบทดสอบ
    score: { type: Number },//คะแนนทั้งหมด
    createdAt: { //สร้างเมื่อ
        type: Date,
        default: Date.now
    },
})

module.exports = mongoose.model('testtask_log', TestTask);