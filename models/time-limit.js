const mongoose = require('mongoose');

const timeLimit = mongoose.Schema({
    time: { type: Number,require: true},//เวลาที่เล่นได้ต่อวัน (นาที)
    game:{type:Number,require: true} //จำนวนข้อที่เล่นได้ต่อวัน
}, {
    timestamps: true,
})

module.exports = mongoose.model('timeLimit', timeLimit);