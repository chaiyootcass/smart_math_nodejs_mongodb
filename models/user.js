const mongoose = require('mongoose');

const User = mongoose.Schema({
    uuid: { type: String, require: true}, //เก็บไอดีจาก Firebase auth 
    username: { type: String, require: true },//เก็บไอดีของ user
    numberident_task_level: { type: Number, default: 1 },//เลเวลของเกมส์ numberident
    counting_task_level: { type: Number, default: 1 },// 
    comparison_task_level: { type: Number, default: 1 },
    addition_task_level: { type: Number, default: 1 },
    ordinal_task_level: { type: Number, default: 1 },
    correspondence_task_level: { type: Number, default: 1 },
    numberline_task_level: { type: Number, default: 1 },
    comparisonnumber_task_level: { type: Number, default: 1 },
    rewards:{type:Object}
}, {
    timestamps: true,
})

module.exports = mongoose.model('User', User);