const mongoose = require('mongoose');

const NumberlineTask = mongoose.Schema({
    userid: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    level: { type: Number },
    time: { type: Number },
    correct: { type: Number },
    fail: { type: Number },
    solve: { type: [Number] },
    answer: { type: [Number] },
    note:{ type:String, default: 'Level up' },
    createdAt: {
        type: Date,
        default: Date.now
    },
})

module.exports = mongoose.model('numberline_log', NumberlineTask);