const express = require('express');
const router = express.Router();
const ComparisonNumberTask = require('../models/comparisonnumber-task');

router.post('/add',(req,res) => {
    ComparisonNumberTask(req.body).save().then((result) => {
        console.log('Add ComparisonNumberTask log',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't post");    
    });
});

module.exports = router;
