const express = require('express');
const router = express.Router();
const CountingTask = require('../models/counting-task');

router.post('/add',(req,res) => {
    CountingTask(req.body).save().then((result) => {
        console.log('Add CountingTask log',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't post");    
    });
});

module.exports = router;
