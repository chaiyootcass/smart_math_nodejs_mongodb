const express = require('express');
const router = express.Router();
const Ordinaltask = require('../models/ordinal-task');

router.post('/add',(req,res) => {
    Ordinaltask(req.body).save().then((result) => {
        console.log('Add Ordinaltask log',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't post");    
    });
});

module.exports = router;
