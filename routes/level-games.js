const express = require('express');
const router = express.Router();
const levelGame = require('../models/level-game');


router.post('/add',(req,res) => {
    let data = req.body;
    levelGame(data).save().then((result) => {
        console.log('new level game :',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't create user");
    });
});

router.post('/update', (req, res) => {
    let data = req.body;
    levelGame.findOneAndUpdate({ game: data.game },
        { $set: { numLevel: data.numLevel, level: data.level,persentPromote:data.persentPromote,minimumExam:data.minimumExam } },{ new: true }).then((result) => {
            if(result ===null){
                throw new Error('Game Not Found');
            }
            console.log('Updated Level!',result._id);
            res.status(201).end()
        }).catch((err) => {
              console.log(err);
              res.status(400).send("Error can't put");
        });
});

router.get('/:gamename',async (req,res) =>  {
    let namegame = req.params.gamename;
    console.log('find : ',namegame);
    let data = await levelGame.findOne({game:namegame});
    res.json(data);
});

module.exports = router;