const express = require('express');
const router = express.Router();
const TestTask = require('../models/test-task');
const User =  require('../models/user');
router.post('/add',(req,res) => {
    let data = req.body;
    TestTask.findOne({userid:data.userid}).then( async (result) => {
        if(result){
            delete data['userid'];
            for(i=0;i<result.log.length;i++){
                if(result.log[i].namegame==data.namegame){
                    console.log(data);
                    result.time-=result.log[i].counttime;
                    result.score-=result.log[i].score;
                    result.log[i]=data;
                    result.time+=data.counttime;
                    result.score+=data.score;
                    result.markModified('log');
                    result.save();
                    console.log('Update TestTask log',result._id);
                    console.log(result.log[i].namegame);
                    res.status(201).end();
                    return;
                }
            }
            result.log.push(data);
            result.time+=data.counttime;
            result.score+=data.score;
            result.markModified('log');
            result.save();
            console.log('Update new TestTask log',result._id);
            res.status(201).end();
            return;
        }else{
            TestTask({userid:data.userid,time:data.counttime,
                score:data.score,log:[{game:data.namegame,ans:data.ans,sol:data.sol,
                    coorect:data.correct,
                    score:data.score,
                    time:data.time,
                    counttime:data.counttime}]}
                ).save().then((result) => {
                console.log('New TestTask log',result._id);
                res.status(201).end()
            }).catch((err) => {
                console.log(err);
                res.status(400).send("Error can't post");    
            });
        }   
    }).catch((err) => {
           console.log(err);
           res.status(400).send("Error can't post");    
    });
   
});


router.get('/get',(req,res) => {
    let username = req.body.username;
    console.log('username ',username);
    User.findOne({username:username}).then((result) => {
        console.log('result ',result);
        if(result){
            TestTask.findOne({userid:result._id}).sort({"createdAt": -1}).then((data) => {
                res.json(data)
            }).catch((err) => {
                res.json({status:'Not found data'});
            });
        }else{
            res.json({status:'Not found user'});
        }
    }).catch((err) => {
        res.status(400).send("Error!");    
    });
});


module.exports = router;
