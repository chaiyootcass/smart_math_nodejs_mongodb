const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Levelgame = require('../models/level-game');
const Timelimit = require('../models/time-limit');
const AdditionTask = require('../models/addition-task');
const ComparisonTask = require('../models/comparison-task');
const ComparisonNumberTask = require('../models/comparisonnumber-task');
const CorrespondenceTask = require('../models/correspondence-task');
const NumberIdenTask = require('../models/number-identification');
const NumberlineTask = require('../models/numberline-task');
const Ordinaltask = require('../models/ordinal-task');

const AllTask = [{
    game: AdditionTask,
    name: 'addition_task'
}, {
    game: ComparisonNumberTask,
    name: 'comparisonnumber_task'
}, {
    game: ComparisonTask,
    name: 'comparison_task',
},
{
    game: CorrespondenceTask,
    name: 'correspondence_task',
},
{
    game: NumberIdenTask,
    name: 'numberident_task',
},
{
    game: NumberlineTask,
    name: 'numberline_task',
},
{
    game: Ordinaltask,
    name: 'ordinal_task',
},
];

router.post('/newusername', (req, res) => {
    User(req.body).save().then((doc) => {
        console.log('newID : ', doc._id);
        res.json({
            id: doc._id
        });
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't create user");
    });
});

router.post('/userdata', (req, res) => {
    console.log('userdata');
    User.findOne({
        uuid: req.body.uuid
    }, {
        __v: 0
    }).then((result) => {
        res.json(result);
    }).catch((err) => {
        console.log(err);
        res.status(400);
    });
});

router.get('/getdata', (req, res) => {
    console.log('getdata');
    Levelgame.find({}, {
        _id: 0,
        __v: 0,
        createdAt: 0,
        updatedAt: 0
    }).then((result) => {
        Timelimit.findOne({}, {
            time: 1,
            _id: 0,
            game: 1
        }).then((limit) => {
            res.json({
                levelgame: result,
                limit
            });
        }).catch((err) => {
            console.log(err);
            res.status(400);
        });
    }).catch((err) => {
        console.log(err);
        res.status(400);
    });
});

router.get('/getstat',async (req, res) => {
    console.log('getstat');
    const user = req.body.userid;
    var count = 0;
    var data = [];
    try {
        AllTask.map((element) => {
            element.game.find({
               userid: user
           }).then((result) => {
               let correct = 0;
               let fail = 0;
               let time = 0;
               result.forEach((data) => {
                   correct += data.correct;
                   fail += data.fail;
                   time += data.time;
               });
               data.push({game: element.name,
                   data: {
                       correct,
                       fail,
                       time
                   }});
                   count++;
                if(AllTask.length==count){
                   res.json(data)
                }
           }).catch((err) => {
            console.err(err);
           })
       });
    } catch (error) {
        res.status(400).send("Error can't get stat");    
    }
});

router.get('/getallstat',async (req, res) => {
    console.log('getstat');
    const user = req.body.userid;
    var count = 0;
    var data = [];
    try {
        AllTask.map((element) => {
            element.game.find().then((result) => {
               let correct = 0;
               let fail = 0;
               let time = 0;
               result.forEach((data) => {
                   correct += data.correct;
                   fail += data.fail;
                   time += data.time;
               });
               data.push({game: element.name,
                   data: {
                       correct,
                       fail,
                       time
                   }});
                   count++;
                if(AllTask.length==count){
                   res.json(data)
                }
           }).catch((err) => {
            console.err(err);
           })
       });
    } catch (error) {
        res.status(400).send("Error can't get stat");    
    }
});

module.exports = router;