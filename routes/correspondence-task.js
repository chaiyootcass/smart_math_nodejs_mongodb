const express = require('express');
const router = express.Router();
const CorrespondenceTask = require('../models/correspondence-task');

router.post('/add',(req,res) => {
    CorrespondenceTask(req.body).save().then((result) => {
        console.log('Add AdditionTask log',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't post");    
    });
});

module.exports = router;
