const express = require('express');
const router = express.Router();
const NumberlineTask = require('../models/numberline-task');

router.post('/add',(req,res) => {
    NumberlineTask(req.body).save().then((result) => {
        console.log('Add NumberlineTask log',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't post");    
    });
});

module.exports = router;
