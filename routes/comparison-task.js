const express = require('express');
const router = express.Router();
const ComparisonTask = require('../models/comparison-task');

router.post('/add',(req,res) => {
    ComparisonTask(req.body).save().then((result) => {
        console.log('Add ComparisonTask log',result._id);
        res.status(201).end()
    }).catch((err) => {
        console.log(err);
        res.status(400).send("Error can't post");    
    });
});

module.exports = router;
